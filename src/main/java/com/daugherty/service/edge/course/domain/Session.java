package com.daugherty.service.edge.course.domain;

import java.util.Date;

/**
 * @author garethdavies
 */
public class Session extends PatchableSession {

    private String course;
    private String student;
    private Date start;
    private Date end;

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

}
