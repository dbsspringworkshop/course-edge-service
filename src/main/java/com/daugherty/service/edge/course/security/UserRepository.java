package com.daugherty.service.edge.course.security;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

	User findByUsername(String username);

	User findById(String id);

	User findByProviderIdAndProviderUserId(String providerId, String providerUserId);
}
