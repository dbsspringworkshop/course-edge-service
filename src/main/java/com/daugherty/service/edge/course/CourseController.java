package com.daugherty.service.edge.course;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.daugherty.service.edge.course.domain.Course;
import com.daugherty.service.edge.course.domain.CourseDomainClient;
import com.daugherty.service.edge.course.domain.Session;
import com.daugherty.service.edge.course.domain.SessionDomainClient;
import com.daugherty.service.edge.course.domain.Student;
import com.daugherty.service.edge.course.domain.StudentDomainClient;
import com.daugherty.service.edge.course.security.User;
import com.daugherty.service.edge.course.security.UserAuthentication;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * @author garethdavies
 */
@RestController
@RequestMapping(value = "/api/courses", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" })
@ExposesResourceFor(CourseResource.class)
@RefreshScope
@EnableBinding(Source.class)
public class CourseController {

    @Autowired
    private CourseDomainClient courseDomainClient;
    @Autowired
    private SessionDomainClient sessionDomainClient;
    @Autowired
    private StudentDomainClient studentDomainClient;
    @Autowired
    private EntityLinks entityLinks;
    @Autowired
    private Source signinSource;
    @Autowired
    private SigninHandler signinHandler;

    @Value("${signin.passphrase}")
    private String signinPassphrase;

    private final Scheduler scheduler = Schedulers.elastic();

    @GetMapping
    public Resources<CourseResource> getCourses(@RequestParam(name = "student", required = false) String student) {
        List<CourseResource> courseResources = null;
        if (StringUtils.isNotBlank(student)) {
            Resources<Resource<Session>> sessions = sessionDomainClient.findSessionsByStudent(student);
            courseResources = sessions.getContent().stream().map(s -> loadCourseForSession(s))
                    .collect(Collectors.toList());

        } else {
            Resources<Resource<Course>> courses = courseDomainClient.getCourses();
            courseResources = courses.getContent().stream().map(e -> toCourseResource(e)).collect(Collectors.toList());
        }
        Link self = entityLinks.linkToCollectionResource(CourseResource.class);
        return new Resources<CourseResource>(courseResources, self);
    }

    @GetMapping("/{id}")
    public Resource<CourseResource> getCourse(@PathVariable("id") String id) {
        Resource<Course> course = courseDomainClient.loadCourse(id);
        CourseResource courseResource = toCourseResource(course);

        Resources<Resource<Session>> sessions = sessionDomainClient.findSessionsByCourse(id);

        // Non-reactive
        for (Resource<Session> session : sessions) {
            StudentSession studentSession = toStudentSession(session);
            courseResource.addStudent(studentSession);
        }

        // Reactive #1
//        Flux.fromIterable(sessions)
//        .log()
//        .map(s -> toStudentSession(s))
//        .subscribe(ss -> courseResource.addStudent(ss));

        // Reactive #2
//        Flux.fromIterable(sessions)
//        .log()
//        .flatMap(this::toStudentSessionMono, 4, 4)
//        .subscribe(ss -> courseResource.addStudent(ss));

        // TODO Explore making course a Mono

        return new Resource<CourseResource>(courseResource);
    }

    @PostMapping(value = "/{courseId}/signins/{sessionId}", produces = "text/plain")
    public ResponseEntity<String> signin(@PathVariable("courseId") String courseId,
            @PathVariable("sessionId") String sessionId, @RequestBody Signin signin) {
        if (!signinPassphrase.equalsIgnoreCase(signin.getConfirmationPhrase())) {
            return new ResponseEntity<String>("Confirmation passphrase does not match", HttpStatus.BAD_REQUEST);
        }

        User user = ((UserAuthentication)SecurityContextHolder.getContext().getAuthentication()).getDetails();

        SigninAttempt signinAttempt = new SigninAttempt();
        signinAttempt.setCourseId(courseId);
        signinAttempt.setSessionId(sessionId);
        signinAttempt.setStudentName(user.getName());
        signinSource.output().send(MessageBuilder.withPayload(signinAttempt).build());

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/signin-stream", headers = "Accept=text/event-stream", produces = "text/event-stream")
    public SseEmitter signinStream(@PathVariable("id") String id) {
        return signinHandler.getSseEmitter(id);
    }

    private String extractIdFrom(ResourceSupport entity) {
        return StringUtils.substringAfterLast(entity.getId().getHref(), "/");
    }

    private Mono<StudentSession> toStudentSessionMono(Resource<Session> entity) {
        return Mono.just(toStudentSession(entity)).subscribeOn(scheduler);
    }

    private StudentSession toStudentSession(Resource<Session> entity) {
        Session session = entity.getContent();

        Resource<Student> student = studentDomainClient.loadStudent(entity.getContent().getStudent());
        return new StudentSession(extractIdFrom(entity), student.getContent().getName(),
                student.getContent().getLineOfService().name(), session.getStart(), session.getEnd(),
                session.getSignedIn());
    }

    private CourseResource loadCourseForSession(Resource<Session> session) {
        Resource<Course> course = courseDomainClient.loadCourse(session.getContent().getCourse());
        CourseResource courseResource = toCourseResource(course);

        courseResource.add(entityLinks.linkForSingleResource(CourseResource.class, extractIdFrom(course))
                .slash("signins")
                .slash(extractIdFrom(session))
                .withRel("signin"));
        // I think this link should be added however the GET /courses endpoint is invoked (i.e. in toCourseResource)
        courseResource.add(entityLinks.linkForSingleResource(CourseResource.class, extractIdFrom(course))
                .slash("signin-stream")
                .withRel("stream"));

        return courseResource;
    }

    private CourseResource toCourseResource(Resource<Course> entity) {
        CourseResource resource = new CourseResource();
        Course entityContent = entity.getContent();
        resource.setName(entityContent.getName());
        resource.setFormat(entityContent.getFormat());
        resource.setLocation(entityContent.getLocation());
        resource.setInstructors(
                entityContent.getInstructors().stream().map(i -> i.getName()).collect(Collectors.toList()));
        resource.add(entityLinks.linkToSingleResource(CourseResource.class, extractIdFrom(entity)));
        return resource;
    }
}
