package com.daugherty.service.edge.course.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.daugherty.service.edge.course.domain.Student;
import com.daugherty.service.edge.course.domain.StudentDomainClient;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private StudentDomainClient studentDomainClient;

    @RequestMapping(value = "/api/users/current", method = RequestMethod.GET)
    public User getCurrent() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if (authentication instanceof UserAuthentication) {
            user = ((UserAuthentication) authentication).getDetails();

            Resource<Student> studentResource = studentDomainClient.loadStudentByTwitterHandle(user.getUsername());
            user.setStudentId(extractIdFrom(studentResource));
            user.setName(studentResource.getContent().getName());
            user.setTwitterHandle(studentResource.getContent().getTwitterHandle());

        } else {
            user = new User(); // anonymous user support
        }
        return user;
    }

    private String extractIdFrom(Resource<Student> entity) {
        return StringUtils.substringAfterLast(entity.getId().getHref(), "/");
    }
}
