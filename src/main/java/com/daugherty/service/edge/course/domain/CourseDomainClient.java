package com.daugherty.service.edge.course.domain;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author garethdavies
 *
 */
@FeignClient(name = "course-domain-service", configuration = HttpBasicSecurityConfiguration.class)
public interface CourseDomainClient {

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    Resources<Resource<Course>> getCourses();

    @RequestMapping(value = "/courses/{id}", method = RequestMethod.GET)
    Resource<Course> loadCourse(@PathVariable("id") String id);

}
