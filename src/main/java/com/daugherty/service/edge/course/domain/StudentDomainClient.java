package com.daugherty.service.edge.course.domain;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author garethdavies
 *
 */
@FeignClient(name = "student-domain-service", configuration = HttpBasicSecurityConfiguration.class)
public interface StudentDomainClient {

    @RequestMapping(value = "/students/{id}", method = RequestMethod.GET)
    Resource<Student> loadStudent(@PathVariable("id") String id);

    @RequestMapping(value = "/students/search/twitterHandle", method = RequestMethod.GET)
    Resource<Student> loadStudentByTwitterHandle(@RequestParam("twitterHandle") String twitterHandle);
}
