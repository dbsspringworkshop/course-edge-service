package com.daugherty.service.edge.course;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author garethdavies
 */
public class CourseResource extends ResourceSupport {

    private String name;
    private String format;
    private String location;
    private List<String> instructors;
    private List<StudentSession> students;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getInstructors() {
        return instructors;
    }

    public void setInstructors(List<String> instructors) {
        this.instructors = instructors;
    }

    public List<StudentSession> getStudents() {
        return students;
    }
    
    public void addStudent(StudentSession student) {
        if (students == null) {
            students = new ArrayList<StudentSession>();
        }
        students.add(student);
    }

}
