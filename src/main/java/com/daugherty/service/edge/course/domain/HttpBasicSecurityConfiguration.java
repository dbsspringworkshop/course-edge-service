/**
 *
 */
package com.daugherty.service.edge.course.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.auth.BasicAuthRequestInterceptor;

/**
 * @author gid1123
 *
 */
@Configuration
public class HttpBasicSecurityConfiguration {

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor(Credentials credentials) {
        return new BasicAuthRequestInterceptor(credentials.getUsername(), credentials.getPassword());
    }

}
