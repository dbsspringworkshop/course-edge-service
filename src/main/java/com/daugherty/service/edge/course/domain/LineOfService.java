package com.daugherty.service.edge.course.domain;

/**
 * @author garethdavies
 *
 */
public enum LineOfService {

  BusinessAnalysis, InformationManagement, ProjectManagement, SoftwareArchitectureAndEngineering

}
