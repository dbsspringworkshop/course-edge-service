package com.daugherty.service.edge.course;

import java.util.Date;

/**
 * @author garethdavies
 *
 */
public class StudentSession {
	
	private final String sessionId;
	private final String name;
	private final String lineOfService;
	private final Date start;
	private final Date end;
	private final Date signin;

	public StudentSession(String sessionId, String name, String lineOfService, Date start, Date end, Date signin) {
		this.sessionId  = sessionId;
		this.name = name;
		this.lineOfService = lineOfService;
		this.start = start;
		this.end = end;
		this.signin = signin;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public String getName() {
		return name;
	}

	public String getLineOfService() {
		return lineOfService;
	}

	public Date getStart() {
		return start;
	}

	public Date getEnd() {
		return end;
	}

	public Date getSignin() {
		return signin;
	}

}
