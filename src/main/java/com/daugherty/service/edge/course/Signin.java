package com.daugherty.service.edge.course;

/**
 * @author garethdavies
 *
 */
public class Signin {

  private String confirmationPhrase;

  public String getConfirmationPhrase() {
    return confirmationPhrase;
  }

  public void setConfirmationPhrase(String confirmationPhrase) {
    this.confirmationPhrase = confirmationPhrase;
  }

}
