package com.daugherty.service.edge.course.domain;

import java.util.Date;

/**
 * @author garethdavies
 *
 */
public class PatchableSession {

  private Date signedIn;

  public Date getSignedIn() {
    return signedIn;
  }

  public void setSignedIn(Date signedIn) {
    this.signedIn = signedIn;
  }

}
