package com.daugherty.service.edge.course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.daugherty.service.edge.course.domain.Credentials;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
@EnableFeignClients
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class CourseEdgeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseEdgeServiceApplication.class, args);
    }

    @ConfigurationProperties("domain.microservice")
    @Bean
    public Credentials domainMicroserviceCredentials() {
        return new Credentials();
    }

    /*
     * The following configuration is necessary because @EnableHypermediaSupport
     * configures its own ObjectMapper which is different from the one
     * configured via application.properties!
     *
     * See https://github.com/spring-projects/spring-hateoas/issues/333.
     */
    private static final String SPRING_HATEOAS_OBJECT_MAPPER = "_halObjectMapper";

    @Autowired
    @Qualifier(SPRING_HATEOAS_OBJECT_MAPPER)
    private ObjectMapper springHateoasObjectMapper;

    @Autowired
    private Jackson2ObjectMapperBuilder springBootObjectMapperBuilder;

    @Bean(name = "objectMapper")
    @Primary
    ObjectMapper objectMapper() {
        this.springBootObjectMapperBuilder.configure(this.springHateoasObjectMapper);

        return springHateoasObjectMapper;
    }

}
