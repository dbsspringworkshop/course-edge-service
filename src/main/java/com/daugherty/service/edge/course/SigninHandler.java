package com.daugherty.service.edge.course;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.daugherty.service.edge.course.domain.PatchableSession;
import com.daugherty.service.edge.course.domain.SessionDomainClient;

import feign.FeignException;

/**
 * @author garethdavies
 */
@EnableBinding(Sink.class)
public class SigninHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(SigninHandler.class);

    @Autowired
    private SessionDomainClient sessionDomainClient;

    private final Map<String, SseEmitter> sseEmittersByCourse = new HashMap<String, SseEmitter>();

    @StreamListener(Sink.INPUT)
    public void handleSignin(SigninAttempt signinAttempt) {
        LOGGER.info("Processing signin for: " + signinAttempt.getStudentName());
        try {
            PatchableSession session = new PatchableSession();
            session.setSignedIn(new Date());
            sessionDomainClient.updateSignedInDate(signinAttempt.getSessionId(), session);
            emitServerSentEvent(signinAttempt);
        } catch (RuntimeException e) {
            if (e.getCause() instanceof FeignException) {
                String causeMessage = e.getCause().getMessage();
                String contentMessage = StringUtils.substringAfter(causeMessage, "content:");
                if (StringUtils.isBlank(contentMessage)) {
                    signinAttempt.setErrorMessage(causeMessage);
                } else {
                    signinAttempt.setErrorMessage(contentMessage);
                }
                emitServerSentEvent(signinAttempt);
            } else {
                signinAttempt.setErrorMessage(e.getMessage());
                emitServerSentEvent(signinAttempt);
            }
        }

    }

    private void emitServerSentEvent(SigninAttempt signinAttempt) {
        SseEmitter sseEmitter = getSseEmitter(signinAttempt.getCourseId());
        try {
            LOGGER.info("Sending SSE for signin. Error message = " + signinAttempt.getErrorMessage());
            sseEmitter.send(signinAttempt, MediaType.APPLICATION_JSON_UTF8);
        } catch (IOException e) {
            LOGGER.warn("Unable to send signin as a Server-Sent Event");
            sseEmitter.completeWithError(e);
        }
    }

    public SseEmitter getSseEmitter(String id) {
        synchronized (sseEmittersByCourse) {
            SseEmitter sseEmitter = sseEmittersByCourse.get(id);
            if (sseEmitter == null) {
                LOGGER.info("Creating a new SseEmitter for course " + id);
                sseEmitter = new SseEmitter(-1L);
                sseEmittersByCourse.put(id, sseEmitter);
                sseEmitter.onCompletion(() -> sseEmittersByCourse.remove(id));
            }
            return sseEmitter;
        }
    }

}
