package com.daugherty.service.edge.course.domain;

import java.util.List;

/**
 * @author garethdavies
 *
 */
public class Course {

  private String name;
  private String format;
  private String location;
  private List<Instructor> instructors;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public List<Instructor> getInstructors() {
    return instructors;
  }

  public void setInstructors(List<Instructor> instructors) {
    this.instructors = instructors;
  }

}
