package com.daugherty.service.edge.course.domain;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author garethdavies
 */
@FeignClient(name = "session-domain-service", configuration = HttpBasicSecurityConfiguration.class)
public interface SessionDomainClient {

    @RequestMapping(value = "/sessions/search/byCourse", method = RequestMethod.GET)
    Resources<Resource<Session>> findSessionsByCourse(@RequestParam("course") String course);

    @RequestMapping(value = "/sessions/search/byStudent", method = RequestMethod.GET)
    Resources<Resource<Session>> findSessionsByStudent(@RequestParam("student") String student);

    @RequestMapping(value = "/sessions/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    void updateSignedInDate(@PathVariable("id") String id, @RequestBody PatchableSession session);

}
