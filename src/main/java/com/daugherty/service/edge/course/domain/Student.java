package com.daugherty.service.edge.course.domain;

/**
 * @author garethdavies
 *
 */
public class Student {

  private String name;
  private LineOfService lineOfService;
  private String twitterHandle;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LineOfService getLineOfService() {
    return lineOfService;
  }

  public void setLineOfService(LineOfService lineOfService) {
    this.lineOfService = lineOfService;
  }

  public String getTwitterHandle() {
    return twitterHandle;
  }

  public void setTwitterHandle(String twitterHandle) {
    this.twitterHandle = twitterHandle;
  }

}
