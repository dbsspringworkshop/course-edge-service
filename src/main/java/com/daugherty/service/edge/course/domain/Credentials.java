/**
 *
 */
package com.daugherty.service.edge.course.domain;

/**
 * @author gid1123
 *
 */
public class Credentials {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
