package com.daugherty.service.edge.course.domain;

/**
 * @author garethdavies
 *
 */
public class Instructor {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
