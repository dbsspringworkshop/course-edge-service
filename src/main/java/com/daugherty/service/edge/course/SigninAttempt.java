package com.daugherty.service.edge.course;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author garethdavies
 *
 */
public class SigninAttempt {

  private String courseId;
  private String sessionId;
  private String studentName;
  private String errorMessage;

  @JsonIgnore
  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  @JsonIgnore
  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public boolean isSuccess() {
    return StringUtils.isBlank(errorMessage);
  }

}
