(function() {	
document.app.directive('modalHeader', function(){
    return {
        template:'<div class="modal-header bg-inverse" style="color: #FFFFFF"><button type="button" class="close" style="color: #FFFFFF" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">{{title}}</h4></div>',
        replace:true,
        restrict: 'E',
        scope: {title:'@'}
    };
});
})();