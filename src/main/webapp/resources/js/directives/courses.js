(function() {	
	document.app.directive('courses', function(){
	  return {
        restrict: 'E',
        scope: {
        	courses: "=",
        	user: "="
        },
        templateUrl : 'resources/html/courses.html'
      }
    });
})();