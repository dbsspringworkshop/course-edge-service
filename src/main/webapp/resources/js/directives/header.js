(function() {	
	document.app.directive('header', function(){
	  return {
        restrict: 'E',
        scope: {
        	authenticated: "=",
        	logout: "&",
        	user: "="
        },
        templateUrl : 'resources/html/header.html'
      }
    });
})();
