(function() {	
	document.app.controller('signinsheetsCtrl', function ($scope, $rootScope, $http, $cookies, $state, toastr) {
		
	    $scope.courses = [];
	    $scope.selectedCourse = null;
	    var evtSource = null; 

		$scope.loadCourses = function() {
			
			var url = '/api/courses';
			
			if($rootScope.authenticated) {
				url = url + "?student=" + $scope.user.studentId;
			}
			
			$http.get(url).success(function (courses) {
				if (courses && courses._embedded && courses._embedded.courseResourceList && courses._embedded.courseResourceList.length > 0) {
					$scope.courses = courses._embedded.courseResourceList;
					$scope.selectedCourse = courses._embedded.courseResourceList[0]
					$scope.onCourseChange();
				}
			});
		};
		
		$scope.onCourseChange = function() {
			var course = $scope.selectedCourse;
			
			evtSource = new EventSource(course._links.stream.href);
			
			evtSource.onmessage = function(response) {
				
				if(response.data){ 
					var message = JSON.parse(response.data);
					if(message.success){
						toastr.success(message.studentName + ' Confirmed');
						$http.get(course._links.self.href).success(function (course) {
							$scope.selectedCourse = course;
						});
					} else {
						var errorMessage = JSON.parse(message.errorMessage)
						for (var i = 0; i < errorMessage.errors.length; i++) {
							toastr.error(errorMessage.errors[i].message, message.studentName);
						}
					}
					
				}
			}
			
			evtSource.onerror = function(e) {
				console.log("Error", e);
			};
			
			if(course && course._links && course._links.self && course._links.self.href) {
				$http.get(course._links.self.href).success(function (course) {
					$scope.selectedCourse = course;	
				});
			}
			

		};
		
		$scope.isSignedIn = function(student) {
			return student.signin && student.signin.length > 0;
		};
		
		$scope.getInstructors = function(course) {
			if(course && course.instructors) {
				return course.instructors.join(", ");
			}
			return "";
		}
		
	    $scope.loadCourses();
		
	});
})();