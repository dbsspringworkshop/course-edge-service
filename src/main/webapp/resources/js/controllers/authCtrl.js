(function() {	
	document.app.controller('authCtrl', function ($scope, $rootScope, $http, $cookies, $state, TokenStorage) {
		$rootScope.authenticated = false;
		$scope.user = null;
		
		$rootScope.$on('$stateChangeStart', function(event, toState) {
		    if (toState.name !== 'home' && !$rootScope.authenticated) {
		        event.preventDefault();
		    	$state.transitionTo('home');
		    }
		});
		
		$scope.init = function () {
			var authCookie = $cookies['AUTH-TOKEN'];
			if (authCookie) {
				$rootScope.authenticated = true;
				TokenStorage.store(authCookie);
				delete $cookies['AUTH-TOKEN'];
			}
			
			var authToken = TokenStorage.retrieve();
			if (authToken) {
				$rootScope.authenticated = true;
				this.getCurrentUser();
			}
		};
		
		$scope.logout = function () {
			// Just clear the local storage
			TokenStorage.clear();
			$rootScope.authenticated = false;
			$state.transitionTo('home');
		};
		
		$scope.getCurrentUser = function() {
			$http.get('/api/users/current').success(function (user) {
				if (user && user.username) {
					$scope.user = user;
					$scope.loadCourses(user.studentId);
				}
			});
		};
		$scope.loadCourses = function(studentId) {
			console.log("TEST - Loading courses. Student Id: ", studentId);
			var url = '/api/courses?student=' + studentId;
			
			$scope.loading = true;
			$http.get(url).success(function (courses) {
				console.log("TEST - Courses loaded", courses);
				if (courses && courses._embedded && courses._embedded.courseResourceList && courses._embedded.courseResourceList.length > 0) {
					$scope.courses = courses._embedded.courseResourceList;
				}
				$scope.loading = false;
			});
		};
	});
})();