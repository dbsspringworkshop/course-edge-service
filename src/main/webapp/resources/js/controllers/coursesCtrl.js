(function() {	
	document.app.controller('coursesCtrl', function ($scope, $rootScope, $http, toastr) {
		
	    $scope.selectedCourse = null;
	    $scope.showCourseSignInModal = false;
	    $scope.formData = {};

	    $scope.getInstructors = function(course) {
			if(course && course.instructors) {
				return course.instructors.join(', ');
			}
			return '';
		};
		
		$scope.onShowCourseSignInModal = function(course) {
			$scope.selectedCourse = course;
			$scope.showCourseSignInModal = true;
		};
		
		$scope.onHideCourseSignInModal = function() {
			$scope.selectedCourse = null;
			$scope.showCourseSignInModal = false;
			$scope.formData = {};
		};
		
		$scope.onSubmitPassphrase = function() {
			var course = $scope.selectedCourse;
			if(course && course._links && course._links.self && course._links.self.href && $scope.formData.confirmationPhrase) {
				var signin = {'confirmationPhrase': $scope.formData.confirmationPhrase};
				$http.post(course._links.signin.href, signin).success(function (courses) {
					toastr.success('Passphrase Submitted')
					$scope.onHideCourseSignInModal()
				}).error(function (message){
					toastr.error(message)
			  }); 
			}
		};	
	});
})();