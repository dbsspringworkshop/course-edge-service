(function() {
	document.app = angular.module('springWorkshopApp',
			[ 'ngCookies', 'ui.router', 'toastr']).factory('TokenStorage', function() {
		var storageKey = 'auth_token';
		return {
			store : function(token) {
				return localStorage.setItem(storageKey, token);
			},
			retrieve : function() {
				return localStorage.getItem(storageKey);
			},
			clear : function() {
				return localStorage.removeItem(storageKey);
			}
		};
	}).config(function($httpProvider, $stateProvider, $urlRouterProvider) {
		$httpProvider.interceptors.push('tokenAuthInterceptor');

		var homeState = {
			name : 'home',
			url : '/home',
			templateUrl : 'resources/html/home.html'
		}

		var coursesState = {
			name : 'courses',
			url : '/courses',
			templateUrl : 'resources/html/myCourses.html'
		}

		var signinSheetState = {
			name : 'signinsheets',
			url : '/signinsheets',
			templateUrl : 'resources/html/signinsheets.html'
		}

		$stateProvider.state(homeState);
		$stateProvider.state(coursesState);
		$stateProvider.state(signinSheetState);
		$urlRouterProvider.otherwise('/home');
	});
})();